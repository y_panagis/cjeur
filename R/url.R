library(RCurl)

request <- function(x, ...) UseMethod("request", x)

request.default <- function(x, ... ){

  args <- list(...)
  h <- basicTextGatherer()
  # curl --header "Content-Type: application/soap+xml;charset=UTF-8;action=\"http://eur-lex.europa.eu/EURLexWebService/doQuery\"" --data @ws.xml http://eur-lex.europa.eu/EURLexWebService

  headerFields = list(
    "Content-Type" = "application/soap+xml;charset=UTF-8;action=\"http://eur-lex.europa.eu/EURLexWebService/doQuery\""
  )
  answer <- curlPerform( url = x,
                         httpheader = headerFields,
                         postfields = toString.XMLNode(args$post_body),
                         writefunction = h$update )
  return (h$value())
}

request.notice <- function(x, ...){
    h <- basicTextGatherer()
    headerFields <- list(
      "Accept" = "application/xml",
      "Accept-Language" = "eng"
    )
    message(x)
    answer <- curlPerform( url = x,
                           httpheader = headerFields,
                           writefunction = h$update )

  return(h$value())
}


CELEX <- function (celex){
  me <- list(
    celex = celex
  )
  class(me) <- "CELEX"
  return(me)
}

# request <- function(celex_obj) UseMethod("request")


request.CELEX <- function(x, ...){
  notice <- paste0("http://eur-lex.europa.eu/legal-content/EN/TXT/XML/?uri=CELEX:", x$celex, "&from=EN")
  class(notice) <- "notice"
  notice_xml <- request( notice )
  return(notice_xml)
}

# Class judgment declaration ----------------------------------------------


judgment <- function(celex, lang){
  me <- list(
    celex = celex,
    lang = lang
  )
  class(me) <- "judgment"
  return(me)
}

# method to download single judgment
dowload.judgment <- function(x, output_path){
  # sleep 1 second before downloading
  Sys.sleep(1)
  judgment_url <- sprintf("http://eur-lex.europa.eu/legal-content/%s/TXT/HTML/?uri=CELEX:%s", x$lang, x$celex)
  # let the filename be <celex>_<lang>.html
  filename <- paste0(paste(x$celex, x$lang, sep = '_'), ".html")
  filename <- file.path(output_path, filename)

  tryCatch(download.file(judgment_url, destfile = filename),
           error = function() message(sprintf("Cound not download judgment with CELEX %s", x$celex))
  )
  return(list(CELEX=x$celex, PATH=filename))
}
